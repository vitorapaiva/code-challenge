<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'api', 'prefix' => 'auth'], function () {
    Route::post('authenticate', 'AuthController@authenticate')->name('api.authenticate');
    Route::post('register', 'AuthController@register')->name('api.register');
});

Route::group(['middleware' => ['api','auth'], 'prefix' => 'auth'], function () {
    Route::get('authenticated', 'AuthController@getAuthenticatedUser')->name('api.authenticated');
});

Route::group(['middleware' => ['api','auth'],'prefix' => 'product'],function (){
    Route::get('get/{product_id}','ProductController@getProduct')->name('product.getProduct');
    Route::post('add/','ProductController@createProduct')->name('product.createProduct');
    Route::put('edit/{product_id}','ProductController@editProduct')->name('product.editProduct');
    Route::delete('delete/{product_id}','ProductController@deleteProduct')->name('product.deleteProduct');
});

Route::group(['middleware' => ['api','auth'],'prefix' => 'order'],function (){
    Route::get('get/{order_id}','OrderController@getOrder')->name('order.getOrder');
    Route::post('add/','OrderController@createOrder')->name('order.createOrder');
    Route::put('edit/{order_id}','OrderController@editOrder')->name('order.editOrder');
});

Route::group(['middleware' => ['api','auth'],'prefix' => 'report'],function (){
    Route::get('average/ticket','ReportController@getAverageTicket');
});
