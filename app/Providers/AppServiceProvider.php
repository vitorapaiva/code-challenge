<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Http\Model\User\UserInterface', 'App\Http\Model\User\UserRepository');
        $this->app->bind('App\Http\Model\Product\ProductModelInterface', 'App\Http\Model\Product\ProductModel');
        $this->app->bind('App\Http\Model\Product\ProductInterface', 'App\Http\Model\Product\ProductApiRepository');
        $this->app->bind('App\Http\Model\Order\OrderModelInterface', 'App\Http\Model\Order\OrderModel');
        $this->app->bind('App\Http\Model\Order\OrderInterface', 'App\Http\Model\Order\OrderRepository');
        $this->app->bind('App\Http\Model\OrderItem\OrderItemModelInterface', 'App\Http\Model\OrderItem\OrderItemModelRepository');
        $this->app->bind('App\Http\Model\OrderItem\OrderItemInterface', 'App\Http\Model\OrderItem\OrderItemRepository');
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
