<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Model\Order\OrderInterface;
use App\Events\OrderSubmitted;
use Auth;


class OrderController extends Controller
{

    private $orderRepo;
    private $user;

    public function __construct(OrderInterface $orderRepo)
    {
        $this->orderRepo=$orderRepo;
        $this->user = Auth::user();
    }

    public function getOrder($order_id){
        $order=$this->orderRepo->getOrder($order_id);

        return response()->json($order);
    }

    public function createOrder(Request $request){
        $request->add(["order_code"],uniqid('ORD'),true);
        $validatedData = $request->validate([
        'order_code' => 'string|unique:order',
        'buyer_name' => 'string|required',
        'order_status' => 'string|required',
        'ship_value' => 'numeric|required',
        'product.*' =>'available'
        'product.*.product_id' => 'numeric|required|exists:product',
        'product.*.product_code' => 'numeric|required',
        'product.*.product_qty' => 'numeric|required',
        'product.*.product_price' => 'numeric|required'
        ]);     

        $order=$this->orderRepo->createOrder($this->user->user_id,$request->except('_token'));
        event(new OrderSubmmited($order));
        return response()->json($order);
    }

    public function editOrder($order_id,Request $request){
        $validatedData = $request->validate([
        'buyer_name' => 'string',
        'order_status' => 'string',
        'ship_value' => 'numeric',
        'product.*' =>'available'
        'product.*.product_id' => 'numeric|exists:product',
        'product.*.product_code' => 'numeric',
        'product.*.product_qty' => 'numeric',
        'product.*.product_price' => 'numeric'
        ]);    
        $order=$this->orderRepo->editOrder($order_id,$request->except('_token'));
        return response()->json($order);
    }
}
