<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Model\Product\ProductInterface;
use Auth;


class ProductController extends Controller
{

    private $productRepo;
    private $user;

    public function __construct(ProductInterface $productRepo)
    {
        $this->productRepo=$productRepo;
        $this->user = Auth::user();
    }

    public function getProduct($product_id){
        $product=$this->productRepo->getProduct($product_id);

        return response()->json($product);
    }

    public function createProduct(Request $request){
        $request->add(["product_code"],uniqid('PRD'),true);
        $validatedData = $request->validate([
        'product_code' => 'string|unique:product',
        'product_name' => 'string|required',
        'product_description' => 'string|required',
        'product_inventory' => 'numeric|required',
        'product_price' => 'numeric|required'
        'product_extra_info' => 'string'
        ]);     

        $product=$this->productRepo->createProduct($this->user->user_id,$request->except('_token'));

        return response()->json($product);
    }

    public function editProduct($product_id,Request $request){
        $validatedData = $request->validate([
        'product_name' => 'string',
        'product_description' => 'string',
        'product_inventory' => 'numeric',
        'product_price' => 'numeric'
        'product_extra_info' => 'string'
        ]);    
        $product=$this->productRepo->editProduct($this->user->user_id,$product_id,$request->except('_token'));
        return response()->json($product);
    }

    public function deleteProduct($product_id){
        $product=$this->productRepo->deleteProduct($this->user->user_id,$product_id);
        return response()->json($product);
    }
}
