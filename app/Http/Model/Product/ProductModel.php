<?php

namespace App\Http\Model\Product;

use Watson\Rememberable\Rememberable;
use Illuminate\Database\Eloquent\Model;

class ProductModel extends Model
{
	use Rememberable;
    protected $table = 'product';
    protected $primaryKey = 'product_id';
    protected $guarded = ['product_id'];
    public $rememberCacheTag = 'product_queries';
    public $rememberFor = 10; // 10 minutes

    public function orderItem()
    {
        return $this->belongsTo('App\Http\Model\OrderItem\OrderItemModel','product_id','product_id');
    }
}
