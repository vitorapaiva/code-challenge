<?php

namespace App\Http\Model\Product;

use App\Http\Model\Product\ProductModel;
use App\Http\Model\Product\ProductInterface;

class ProductRepository implements ProductInterface
{
	private $model;
    
    public function __construct(ProductModel $model){
    	$this->model=$model;
    }

    public function getAllProduct(){
    	return $this->model->productBy('product_id')->get();
    }

    public function getProduct($product_id){
    	return $this->model->where('product_id',$product_id)->first();
    }

    public function createProduct($array){
        $result = $this->model->create($array);
        return $result;
    }

    public function editProduct($product_id,$array){
    	$product=$this->getProduct($product_id);
        if(!is_null($product)){            
            $product->fill($array);
            $result = $product->save();
            return $result;
        }
        return false;
    }

    public function deleteProduct($company_id,$product_id){
    	$product=$this->getProduct($company_id,$product_id);
        if(!is_null($product)){            
            $result = $product->delete();
            return $result;
        }
        return false;
    }
}
