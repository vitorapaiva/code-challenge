<?php

namespace App\Http\Model\Order;

use Watson\Rememberable\Rememberable;
use Illuminate\Database\Eloquent\Model;

class OrderModel extends Model
{
    use Rememberable;
	protected $table = 'order';
    protected $primaryKey = 'order_id';
    protected $guarded = ['order_id'];
    public $rememberCacheTag = 'order_queries';
    public $rememberFor = 10; // 10 minutes

    public function orderItem()
    {
        return $this->hasOne('App\Http\Model\OrderItem\OrderItemModel','order_id','order_id');
    }
}
