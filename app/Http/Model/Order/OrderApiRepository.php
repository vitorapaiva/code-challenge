<?php

namespace App\Http\Model\Order;

use App\Http\Model\Order\OrderModelInterface;

class OrderApiRepository implements OrderInterface
{
	private $model;
    
    public function __construct(OrderModelInterface $model){
    	$this->model=$model;
    }

    public function getAllOrder(){
    	return $this->model->orderBy('order_id')->get();
    }

    public function getOrder($order_id){
    	return $this->model->where('order_id',$order_id)->first();
    }

    public function createOrder($array){
        $result = $this->model->create($array);
        return $result;
    }

    public function editOrder($order_id,$array){
    	$order=$this->getOrder($order_id);
        if(!is_null($order)){            
            $order->fill($array);
            $result = $order->save();
            return $result;
        }
        return false;
    }

    public function deleteOrder($company_id,$order_id){
    	$order=$this->getOrder($company_id,$order_id);
        if(!is_null($order)){            
            $result = $order->delete();
            return $result;
        }
        return false;
    }
}
