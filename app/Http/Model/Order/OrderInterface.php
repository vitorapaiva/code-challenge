<?php

namespace App\Http\Model\Order;



interface OrderInterface
{
	
	public function getAllOrder();
	public function getOrder($order_id);
	public function createOrder(array $array_data);
	public function editOrder($order_id,array $array_data);
}
