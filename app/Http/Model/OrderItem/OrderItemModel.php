<?php

namespace App\Http\Model\OrdemItem;

use Watson\Rememberable\Rememberable;
use Illuminate\Database\Eloquent\Model;

class OrdemItemModel extends Model
{
	use Rememberable;
	protected $table = 'order_item';
    protected $primaryKey = 'order_item_id';
    protected $guarded = ['order_item_id'];
    public $rememberCacheTag = 'order_item_queries';

    public function order()
    {
        return $this->belongsTo('App\Http\Model\Order\OrderModel', 'order_item_id');
    }

    public function product()
    {
        return $this->hasOne('App\Http\Model\Product\ProductModel', 'product_id');
    }

    public function clearCache(){
        OrdemItemModel::flushCache();
    }
}
