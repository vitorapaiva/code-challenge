<?php

namespace App\Http\Model\OrdemItem;

use App\Http\Model\OrdemItem\OrdemItemModel;

class OrdemItemApiRepository implements OrdemItemInterface
{
	private $model;
    
    public function __construct(OrdemItemModel $model){
    	$this->model=$model;
    }

    public function createOrdemItem($array){
        return $this->model->create($array);
    }

}
