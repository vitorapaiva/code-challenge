<?php

namespace Tests\Feature;

use App\Http\Model\User\UserModel;
use App\Http\Model\Product\ProductModel;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tymon\JWTAuth\Facades\JWTAuth;

class ProductTest extends TestCase
{

    use RefreshDatabase;

    protected $user;

    /**
     * Create user and get token
     * @return string
     */
    protected function authenticate(){
        $data = [
            'email' => 'teste@gmail.com',
            'name' => 'Teste',
            'password' => 'teste1234',
            'password_confirmation' => 'teste1234'
        ];

        //Send post request
        $response = $this->json('POST',route('api.register'),$data);
        return $response->getData()->token;
    }

    public function testCreate()
    {
        //login
        $token = $this->authenticate();

        
        //create product
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '. $token,
        ])->json('POST',route('product.createProduct'),[
            'products_name' => 'Teste',
            'products_email' => 'teste-product@email.com',
            'products_fee' => 10.2,
        ]);
        $response->assertStatus(200);
    }

    public function testUpdate(){
        
        //login
        $token = $this->authenticate();
        
        //create product returning its data
        $product = $this->withHeaders([
            'Authorization' => 'Bearer '. $token,
        ])->json('POST',route('product.createProduct'),[
            'products_name' => 'Teste',
            'products_email' => 'teste-product@email.com',
            'products_fee' => 10.2,
        ])->getData();
        //edit the product
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '. $token,
        ])->json('PUT',route('product.editProduct',['products_id' => $product->products_id]),[
            'products_name' => 'Teste Novo Nome',
        ]);
        
        $response->assertStatus(200);
        
        $productUpdated=ProductModel::find($product->products_id);
       
        $this->assertEquals('Teste Novo Nome',$productUpdated->products_name);
    }

    public function testDelete(){
        //login
        $token = $this->authenticate();
        
        //create product returning its data
        $product = $this->withHeaders([
            'Authorization' => 'Bearer '. $token,
        ])->json('POST',route('product.createProduct'),[
            'products_name' => 'Teste',
            'products_email' => 'teste-product@email.com',
            'products_fee' => 10.2,
        ])->getData();

        $response = $this->withHeaders([
            'Authorization' => 'Bearer '. $token,
        ])->json('DELETE',route('product.deleteProduct',['products_id' => $product->products_id]));
        
        $response->assertStatus(200);
        
        $productDeleted=ProductModel::find($product->products_id);
        
        $this->assertEquals(null,$productDeleted);
    }
}