<?php

namespace Tests\Feature;

use App\Http\Model\User\UserModel;
use App\Http\Model\Order\OrderModel;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tymon\JWTAuth\Facades\JWTAuth;

class CompanyTest extends TestCase
{

    use RefreshDatabase;

    protected $user;

    /**
     * Create user and get token
     * @return string
     */
    protected function authenticate(){
        $data = [
            'email' => 'teste@gmail.com',
            'name' => 'Teste',
            'password' => 'teste1234',
            'password_confirmation' => 'teste1234'
        ];

        //Send post request
        $response = $this->json('POST',route('api.register'),$data);
        return $response->getData()->token;
    }

    public function createOrder()
    {
        //login
        $token = $this->authenticate();

        
       $response = $this->withHeaders([
            'Authorization' => 'Bearer '. $token,
        ])->json('POST',route('order.createOrder'),[
            'order_code' => 'ORD123',
            'buyer_name' => 'Teste'
        ]);
        
        $response->assertStatus(200);

        $this->assertEquals(10.2,$response->getData()->total_cost);
    }
}